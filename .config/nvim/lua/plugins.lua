vim.cmd([[packadd packer.nvim]])

return require("packer").startup(function(use)
	-- packer
	use("wbthomason/packer.nvim")
	-- markdown:
	-- https://jdhao.github.io/2019/01/15/markdown_edit_preview_nvim/
	use("junegunn/goyo.vim")
	use("junegunn/limelight.vim")
	use("godlygeek/tabular")
	use({
		"iamcco/markdown-preview.nvim",
		run = function()
			vim.fn["mkdp#util#install"]()
		end,
	})
	-- multi-cursors:
	use("mg979/vim-visual-multi")
	-- autocompletion:
	use("honza/vim-snippets") -- https://github.com/honza/vim-snippets
	-- pre-visualization:
	use("sharkdp/fd")
	use("nvim-lua/plenary.nvim")
	use("nvim-telescope/telescope.nvim")
	use("nvim-telescope/telescope-fzf-native.nvim")
	-- editor config:
	use("editorconfig/editorconfig-vim")
	-- tdd for vim plugins:
	use("junegunn/vader.vim")
	--
	-- In the future [I may like to use taskwiki](https://joshua.haase.mx/techno/c%c3%b3mo-trabajo/ )
	-- ([taskwiki](https://github.com/tools-life/taskwiki))
	-- TabNine
	use({ "neoclide/coc.nvim", branch = "release" })
	-- https://github.com/tzachar/cmp-tabnine#install
	-- use "hrsh7th/nvim-cmp"
	-- use {'tzachar/cmp-tabnine', run='./install.sh', requires = 'hsh7th/nvim-cmp'}
end)
